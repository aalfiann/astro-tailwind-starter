export const siteName = 'Astro'
export const siteDescription = 'Astro is new agnostic javascript framework.'
export const siteEmail = 'aalfiann@gmail.com'
export const itemPerPage = 5
