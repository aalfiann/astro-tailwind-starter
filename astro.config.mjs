import { defineConfig } from 'astro/config';
import tailwind from '@astrojs/tailwind';
import sitemap from "@astrojs/sitemap";
import compress from "astro-compress";

// https://astro.build/config
export default defineConfig({
  site: 'https://example.com',
  integrations: [tailwind(), sitemap({
    entryLimit: 10000,
    changefreq: 'weekly',
    priority: 0.7,
    lastmod: new Date()
  }), compress()]
});
