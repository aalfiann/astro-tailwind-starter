# Astro JS + Tailwind CSS Starter Template
Here is just an Astro JS + Tailwind CSS Starter Template.

### Features
- Basic Component UI
- Index and About Page
- Content
- SEO Ready
- Sitemap and RSS Feed
- View Transitions
- Light/Dark mode
- Auto Prettier
- Compress

### Specs
- Astro JS
- Tailwind CSS
- Typescript

### Requirement
- NodeJS v18

### Extension Tools VSCode
- Astro
- EditorConfig
- Tailwind CSS Intellisense

### Usage

```sh
npm install
```

### 🚀 Project Structure

Inside of your Astro project, you'll see the following folders and files:

```text
/
├── public/
├── src/
│   ├── components/
│   ├── content/
│   ├── layouts/
│   └── pages/
│       └── blog/
├── astro.config.mjs
├── package.json
└── README.md
```

Astro looks for `.astro` or `.md` files in the `src/pages/` directory. Each page is exposed as a route based on its file name.

There's nothing special about `src/components/`, but that's where we like to put any Astro/React/Vue/Svelte/Preact components. Default is no using any UI Framework, still purely using Astro JS and Tailwind CSS.

Any static assets, like images, can be placed in the `public/` directory.

### 🧞 Commands

All commands are run from the root of the project, from a terminal:

| Command                   | Action                                           |
| :------------------------ | :----------------------------------------------- |
| `npm install`             | Installs dependencies                            |
| `npm run dev`             | Starts local dev server at `localhost:4321`      |
| `npm run build`           | Build your production site to `./dist/`          |
| `npm run preview`         | Preview your build locally, before deploying     |
| `npm run astro ...`       | Run CLI commands like `astro add`, `astro check` |
| `npm run astro -- --help` | Get help using the Astro CLI                     |

### 👀 Want to learn more?

Feel free to check [our documentation](https://docs.astro.build) or jump into our [Discord server](https://astro.build/chat).
